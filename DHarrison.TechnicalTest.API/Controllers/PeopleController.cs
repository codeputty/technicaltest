﻿using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DHarrison.TechnicalTest.API.Core.Data;
using DHarrison.TechnicalTest.API.Core.Domain;

namespace DHarrison.TechnicalTest.API.Controllers
{
    public class PersonController : ApiController
    {
        private readonly IPeopleRepository _peopleRepository;

        public PersonController(IPeopleRepository peopleRepository)
        {
            _peopleRepository = peopleRepository;
        }

        public HttpResponseMessage Get(int id)
        {
            Person person = _peopleRepository.GetById(id);
            if (person == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, string.Format(CultureInfo.InvariantCulture, "No record found for id {0}", id));
            }
            return Request.CreateResponse(HttpStatusCode.OK, person);
        }

        public HttpResponseMessage Post(Person person)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            _peopleRepository.Add(person);
            _peopleRepository.SubmitChanges();
            return Request.CreateResponse(HttpStatusCode.Created);
        }
    }
}
