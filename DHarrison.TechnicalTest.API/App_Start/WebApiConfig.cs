﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Routing;

namespace DHarrison.TechnicalTest.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            MapPeopleRoute(config);
        }

        public static IHttpRoute MapPeopleRoute(HttpConfiguration config)
        {
            return config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "myservice/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }

        public static void SetupFormatters(HttpConfiguration config)
        {
            config.Formatters.JsonFormatter.AddQueryStringMapping("format", "json", "application/json");
            config.Formatters.XmlFormatter.AddQueryStringMapping("format", "xml", "text/xml");
            config.Formatters.XmlFormatter.UseXmlSerializer = true;
        }
    }
}
