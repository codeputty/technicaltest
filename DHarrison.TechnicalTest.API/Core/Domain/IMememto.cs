﻿using System.Collections.Generic;

namespace DHarrison.TechnicalTest.API.Core.Domain
{
    public interface IMemento
    {
        Dictionary<string, string> GetState();
        void SetState(Dictionary<string, string> state);
    }
}
