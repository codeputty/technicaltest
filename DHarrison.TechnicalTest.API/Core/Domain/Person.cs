﻿using System.Runtime.Serialization;
using DHarrison.TechnicalTest.API.Core.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace DHarrison.TechnicalTest.API.Core.Domain
{
    public class Person : IEntity
    {
        public int Id { get; set; }

         [Required] 
        public string GivenName { get; set; }

        public string MiddleNames { get; set; }

         [DataMember(IsRequired = true)] 

        public string FamilyName { get; set; }

        [DataMember(IsRequired = true)]
        public DateTime DateOfBirth { get; set; }

        public DateTime? DateOfDeath { get; set; }

        [DataMember(IsRequired = true)]
        public double? Height { get; set; }

        [Required]
        public string PlaceOfBirth { get; set; }

        [Required]
        public string TwitterId { get; set; }

        private int? _age;

        public int Age
        {
            get
            {
                if (!_age.HasValue)
                {
                    _age = (DateOfDeath.HasValue ? DateOfDeath.Value : DateTime.Now).Year - DateOfBirth.Year;

                }
                return _age.Value;
            }
            set { _age = value; }
        }


        #region IMemento members

        public Dictionary<string, string> GetState()
        {
            return new Dictionary<string, string>()
                {
                    {"id", Id.ToString(CultureInfo.InvariantCulture)},
                    {"familyName", FamilyName},
                    {"middleNames", MiddleNames},
                    {"givenName", GivenName},
                    {"height", Height.HasValue ? Height.Value.ToString("0.00") : string.Empty},
                    {"placeOfBirth", PlaceOfBirth},
                    {"twitterId", TwitterId},
                    {"dateOfBirth", DateOfBirth.ToString("yyyy-MM-dd")},
                    {"dateOfDeath", DateOfDeath.HasValue ? DateOfDeath.Value.ToString("yyyy-MM-dd") : string.Empty}
                };
        }

        public void SetState(Dictionary<string, string> state)
        {
            Id = int.Parse(state["id"]);
            FamilyName = state["familyName"];
            MiddleNames = state["middleNames"];
            GivenName = state["givenName"];
            PlaceOfBirth = state["placeOfBirth"];
            TwitterId = state["twitterId"];
            DateOfBirth = DateTime.ParseExact(state["dateOfBirth"], "yyyy-MM-dd", CultureInfo.InvariantCulture);
            
            var height = state["height"];
            Height = string.IsNullOrWhiteSpace(height) ? (double?)null : double.Parse(height);
            
            var dateOfDeath = state["dateOfDeath"];
            DateOfDeath = string.IsNullOrEmpty(dateOfDeath) ? (DateTime?) null : DateTime.ParseExact(dateOfDeath, "yyyy-MM-dd", CultureInfo.InvariantCulture);
        }
        
        #endregion
    }
}
