﻿using DHarrison.TechnicalTest.API.Core.Domain;

namespace DHarrison.TechnicalTest.API.Core.Data
{
    public interface IPeopleRepository
    {
        Person GetById(int id);

        void Add(Person person);

        void SubmitChanges();
    }
}
