using System.Linq;
using DHarrison.TechnicalTest.API.Core.Domain;

namespace DHarrison.TechnicalTest.API.Core.Data
{
    public class PeopleRepository : IPeopleRepository
    {
        private readonly PropertyFileDataStore<Person> _dataStore;
        
        public PeopleRepository(PropertyFileDataStore<Person> dataStore)
        {
            _dataStore = dataStore;
        }

        public Person GetById(int id)
        {
            return _dataStore.Items.FirstOrDefault(x => x.Id.Equals(id));
        }

        public void Add(Person person)
        {
            if (!_dataStore.Items.Any())
            {
                person.Id = 1;
            }
            else
            {
                person.Id = _dataStore.Items.Max(x => x.Id) + 1;
            }
            _dataStore.Add(person);
        }

        public void SubmitChanges()
        {
            _dataStore.SubmitChanges();
        }

    }
}