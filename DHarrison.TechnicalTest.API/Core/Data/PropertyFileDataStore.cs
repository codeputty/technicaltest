﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace DHarrison.TechnicalTest.API.Core.Data
{
    public class PropertyFileDataStore<T> where T:class, IEntity, new()
    {
        private readonly DirectoryInfo _dataPath;
        private List<T> _items; 


        public PropertyFileDataStore(IEnumerable<T> items)
        {
            _items = items.ToList();
        }

        public PropertyFileDataStore(string path)
        {
            Debug.Assert(path != null);
            Debug.Assert(Directory.Exists(path));
            _dataPath = new DirectoryInfo(path);
            LoadData();
        }

        public IQueryable<T> Items
        {
            get { return _items.AsQueryable(); }
        }
        
        public void SubmitChanges()
        {
            SaveToDisk();
        }


        public void Add(T item)
        {
            _items.Add(item);
        }

        private void LoadData()
        {
            var items = new List<T>();

            foreach (var file in _dataPath.GetFileSystemInfos("*.txt"))
            {
                var state = ReadPropertyFile(file.FullName);
                var item = new T();
                item.SetState(state);
                items.Add(item);
            }
            _items = items;
        }

        private Dictionary<string, string> ReadPropertyFile(string file)
        {
            var lines = File.ReadAllLines(file);
            return lines.Select(line => line.Split('=')).ToDictionary(parts => parts[0], parts => parts[1]);
        }

        private void WritePropertyFile(string file, Dictionary<string,string> state)
        {
            var lines = state.Select(x => string.Format("{0}={1}", x.Key, x.Value));
            File.WriteAllLines(file,lines);
        }

        private void SaveToDisk()
        {
            foreach(var item in _items)
            {
                var outputFile = string.Format("{0}-{1}.txt", typeof(T).Name, item.Id);
                var state = item.GetState();
                WritePropertyFile(Path.Combine(_dataPath.FullName,outputFile), state);
            }
        }

    }
}