﻿using DHarrison.TechnicalTest.API.Core.Domain;

namespace DHarrison.TechnicalTest.API.Core.Data
{
    public interface IEntity : IMemento
    {
        int Id { get; set; }
    }
}
