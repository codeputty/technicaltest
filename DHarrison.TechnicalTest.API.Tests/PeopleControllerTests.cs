﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Hosting;
using System.Web.Http.Routing;
using DHarrison.TechnicalTest.API.Controllers;
using DHarrison.TechnicalTest.API.Core.Data;
using DHarrison.TechnicalTest.API.Core.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Net.Http.Headers;

namespace DHarrison.TechnicalTest.API.Tests
{
    [TestClass]
    public class PeopleControllerTests
    {

        public PersonController CreateMockedController(IPeopleRepository repository,  string url, HttpMethod httpMethod)
        {
            var controller = new PersonController(repository);
            var config = new HttpConfiguration();
            var request = new HttpRequestMessage(httpMethod,url);
            
            WebApiConfig.SetupFormatters(config);
            var route = WebApiConfig.MapPeopleRoute(config);
            var routeData = new HttpRouteData(route, new HttpRouteValueDictionary { { "controller", "person" } });

            controller.ControllerContext = new HttpControllerContext(config, routeData, request);
            controller.Request = request;
            controller.Request.Properties[HttpPropertyKeys.HttpConfigurationKey] = config;

            return controller;
        }

        [TestMethod]
        public void Get_ValidId_FindsPersonAndReturnsOK()
        {
            // arrange
            var expected = new Person() {Id = 1, GivenName = "Daryl", MiddleNames = "Robert Clark", FamilyName = "Harrison"};
            var mockRepo = new Mock<IPeopleRepository>();
            mockRepo.Setup(x => x.GetById(1)).Returns(expected);

            var controller = CreateMockedController(mockRepo.Object, "http://localhost:34136/api/people/1", HttpMethod.Get);

            // act
            var response = controller.Get(1);
            var actual = ((ObjectContent<Person>) response.Content).Value;

            // assert

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            mockRepo.Verify(x=>x.GetById(1),Times.Once());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Get_InvalidId_ReturnsNotFound()
        {
            // arrange
            var person = new Person() { Id = 1, GivenName = "Daryl", MiddleNames = "Robert Clark", FamilyName = "Harrison" };
            var mockRepo = new Mock<IPeopleRepository>();
            mockRepo.Setup(x => x.GetById(1)).Returns(person);

            var controller = CreateMockedController(mockRepo.Object, "http://localhost:34136/api/people/2", HttpMethod.Get);

            // act
            var actual = controller.Get(2);

            // assert
            Assert.AreEqual(actual.StatusCode, HttpStatusCode.NotFound);
            mockRepo.Verify(x=>x.GetById(2),Times.Once());
        }

        [TestMethod]
        public void Get_FormatXml_ReturnsContentTypeXml()
        {
            // arrange
            var person = new Person() { Id = 1, GivenName = "Daryl", MiddleNames = "Robert Clark", FamilyName = "Harrison" };
            var mockRepo = new Mock<IPeopleRepository>();
            mockRepo.Setup(x => x.GetById(1)).Returns(person);

            // act
            var controller = CreateMockedController(mockRepo.Object, "http://localhost:34136/api/people/1?format=xml", HttpMethod.Get);
            var response = controller.Get(1);

            // assert
            Assert.AreEqual(response.Content.Headers.ContentType.MediaType, "text/xml");
        }

        [TestMethod]
        public void Get_FormatJson_ReturnsContentTypeJson()
        {
            // arrange
            var person = new Person() { Id = 1, GivenName = "Daryl", MiddleNames = "Robert Clark", FamilyName = "Harrison" };
            var mockRepo = new Mock<IPeopleRepository>();
            mockRepo.Setup(x => x.GetById(1)).Returns(person);

            // act
            var controller = CreateMockedController(mockRepo.Object, "http://localhost:34136/api/people/1?format=json", HttpMethod.Get);
            var response = controller.Get(1);

            // assert
            Assert.AreEqual(response.Content.Headers.ContentType.MediaType, "application/json");
        }

        [TestMethod]
        public void Post_ValidPerson_ReturnsCreatedAndPersistsToRepository()
        {
            // arrange
            var validPerson = new Person() { GivenName = "Daryl", MiddleNames = "Robert Clark", FamilyName = "Harrison", DateOfBirth = new DateTime(1981, 3, 6), Height = 1.8, PlaceOfBirth = "Vancouver" };
            var mockRepo = new Mock<IPeopleRepository>();
            var controller = CreateMockedController(mockRepo.Object, "http://localhost:34136/api/people/", HttpMethod.Post);

            // act
            var response = controller.Post(validPerson);

            // assert
            mockRepo.Verify(x => x.Add(validPerson), Times.Once());
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }
        
        [TestMethod]
        [Ignore]
        // ignored for now - ModelState validation is a bit tricky to mock
        public void Post_InvalidPerson_ReturnsBadRequestAndDoesNotPersistToRepository()
        {
            // arrange
            var invalidPerson = new Person() { GivenName = "Incomplete Record"};
            var mockRepo = new Mock<IPeopleRepository>();
            var controller = CreateMockedController(mockRepo.Object, "http://localhost:34136/api/people/", HttpMethod.Post);

            // act
            var response = controller.Post(invalidPerson);

            // assert
            mockRepo.Verify(x => x.Add(invalidPerson), Times.Never());
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

    }
}
