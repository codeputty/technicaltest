
Daryl Harrison technical excercise 
dharrison@gmail.com - 07445 310789

Implementation notes:
 - By default, WebApi uses Content-type / Accept headers to determine input/output content types.  
   - I've added an optional ?format=xml|json in WebApiConfig.SetupFormatters
 - Used the memento pattern to load/save Person state.  DataStore could easily be expanded to add new domain objects that implement IMemento
 - Used the repository pattern for data access.  SubmitChanges() is exposed so that the caller can manage the unit of work
 - The test 'Post_InvalidPerson_ReturnsBadRequestAndDoesNotPersistToRepository' has been ignored.  The test relies on ModelState validation.  
     - It works correctly in the application itself, but I did not have enough time to investigate/implement model validation in the test.
 

 - Libraries/tools used:
    - WebApi
	- MSTest
	- Ninject (dependency injection)
	- Mock
	- Resharper
	- VS 2012